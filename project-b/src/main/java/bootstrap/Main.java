package bootstrap;

import java.math.BigDecimal;

public class Main {

    public static void main(String[] args) {
        System.out.println(sum(new BigDecimal(10)));
    }

    public static BigDecimal sum(BigDecimal... args) {
        BigDecimal returnVal = new BigDecimal(0);
        for (BigDecimal bd : args) {
            returnVal = returnVal.add(bd);
        }
        return returnVal;
    }
}
